BounceAnimation = {
	keyframes: [
		{timeMoment: 0, percent: 100},
		{timeMoment: 55, percent: -5},
		{timeMoment: 85, percent: 3},
		{timeMoment: 95, percent: -1},
		{timeMoment: 100, percent: 0}
	],

	create: function(name, property, startValue, unit) {
		var settings = {
			name: name
		};
		this.keyframes.forEach(function(keyframe, index, keyframes) {
			var keyframePropertyValue = {};
			keyframePropertyValue[property] = (startValue * keyframe.percent / 100.0) + unit;
			settings[keyframe.timeMoment + '%'] = keyframePropertyValue;
		});
		$.keyframe.define([settings]);
	}
};

TasksAnimation = {
	tasksWidth: 195,
	create: function() {
		var windowsWidth = $(window).width();
		var tasksRightOffset = 	windowsWidth -
								$('.svg_container_content_tasks').position().left -
								this.tasksWidth;
		$.keyframe.define([
			{
				name: 'tasks-animation',
				'0%': {'transform': 'translateX(' + tasksRightOffset + 'px) scale(.1, .1)', 'transform-origin': '85% 50%'},
				'70%': {'transform': 'translateX(' + tasksRightOffset + 'px) scale(.1, .1)', 'transform-origin': '85% 50%'},
				'78%': {'transform': 'translateX(-50px) scale(.1, .1)', 'transform-origin': '50% 50%'},
				'92%': {'transform': 'translateX(0px) scale(.1, .1)', 'transform-origin': '50% 50%'},
				'96%': {'transform': 'rotate(90deg)'},
				'100%': {}
			}
		]);
	}
}

MainScreen = {
	elements: [
		{ path: 'svg/effect.svg', delayMs: '0', containerSelector: '.svg_container_content_effect'},
		{ path: 'svg/creative.svg', delayMs: '3300', containerSelector: '.svg_container_content_creative'},
		{ path: 'svg/iweb.svg', delayMs: '3800', containerSelector: '.svg_container_content_web'},
		{ path: 'svg/technology.svg', delayMs: '4350', containerSelector: '.svg_container_content_technology'},
		{ path: 'svg/dlya.svg', delayMs: '3200', containerSelector: '.svg_container_content_for'},
		{ path: 'svg/zadach.svg', delayMs: '0', containerSelector: '.svg_container_content_tasks'},
		{ path: 'svg/resheniya.svg', delayMs: '5850', containerSelector: '.svg_container_content_solutions'},
		{ path: 'svg/bussines.svg', delayMs: '5850', containerSelector: '.svg_container_content_business'},
		{ path: 'svg/folio.svg', delayMs: '6500', containerSelector: '.svg-container__portfolio-wrapper'},
		{ path: 'svg/re-logo.svg', delayMs: '11200', containerSelector: '.logo-container'}
	],

	contactUsElements: [
		{delayMs: 12000, selector: '.contact-us__point_size_small'},
		{delayMs: 12500, selector: '.contact-us__point_size_medium'},
		{delayMs: 13000, selector: '.contact-us__point_size_big'},
		{delayMs: 13500, selector: '.contact-us__button'}
	],

	screens: undefined,

	currentScreen: 0,

	whichAnimationEvent: function() {
		var t,
		el = document.createElement("fakeelement");

		var animations = {
			"animation"      : "animationend",
			"OAnimation"     : "oAnimationEnd",
			"MozAnimation"   : "animationend",
			"WebkitAnimation": "webkitAnimationEnd"
		}

		for (t in animations){
			if (el.style[t] !== undefined){
				return animations[t];
			}
		}
	},

	switchScreen: function() {
		var screenCount = this.screens.length;
		var nextScreen = (this.currentScreen + 1) % screenCount;
		var elementsToHide = this.screens[this.currentScreen].slice().reverse();
		var elementsToShow = this.screens[nextScreen];
		var delay = 0;
		var delayStep = 200;
		elementsToHide.forEach(function(element, index, elements) {
			setTimeout(function() {
				element.addClass('hide-down');
			}, delay);
			delay += delayStep;
		});
		var lastElementToHide = elementsToHide.slice(-1)[0];
		function hideElements() {
			elementsToHide.forEach(function(element, index, elements) {
				element.addClass('hide');
				element.removeClass('hide-down');
			});
		};
		var delay = 0;
		var animationEvent = this.whichAnimationEvent();
		var showStart = 300 + delayStep * elementsToHide.length;
		setTimeout(function() {
			hideElements();
			setTimeout(function() {
				elementsToShow.forEach(function(element, index, elements) {
					setTimeout(function() {
						element.addClass('show-up');
						element.removeClass('hide');
						element.one(animationEvent, function() {
							element.removeClass('show-up');
						});
					}, delay);
					delay += delayStep;
				});
			}, 20);
		}, showStart);
		this.currentScreen = nextScreen;
	},

	init: function() {
		this.elements.forEach(function(element, i, elements) {
			setTimeout(function() {
				var container = $(element.containerSelector);
				container.append(
					'<img class="svg_container__content" src="' + element.path + '" >'
				);
			}, element.delayMs);
		});
		this.contactUsElements.forEach(function(element, i, elements) {
			setTimeout(function() {
				$(element.selector).css('display', 'inline-block');
			}, element.delayMs);
		});
		var contactUsBlock = $('.contact-us');
		$('.contact-us__button').click(function() {
			contactUsBlock.addClass('smooth-hide');
			contactUsBlock.one('transitionend', function() {
				contactUsBlock.addClass('hide');
			});
			$('#svgs .svg_container__content').css('animation', 'none');
			MainScreen.switchScreen();
		});
		$('.form-panel__close').click(function() {
			contactUsBlock.removeClass('hide');
			//без задержки не отрабатывает transitionend
			setTimeout(function() {
				contactUsBlock.addClass('smooth-show');
				contactUsBlock.one('transitionend', function() {
					contactUsBlock.removeClass('smooth-show');
					contactUsBlock.removeClass('smooth-hide');
				});
				MainScreen.switchScreen();
			}, 20);
		});
		this.screens = [
			[$('#svgs'), $('.svg_container_content_portfolio')],
			[$('.form'), $('.form-panel')]
		];
	}
};

$(function() {
	var technologyContainer = $('.svg_container_content_technology');
	BounceAnimation.create(	'technology-animation',
							'top',
							-technologyContainer.position().top,
							'px');
	TasksAnimation.create();
	MainScreen.init();

	var containerElement = $('.container');
	var headElement = $('.head');
	headElement.height(containerElement.position().top);
});